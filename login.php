<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login Page</title>
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
  </head>
  <body>

    <!--form area start-->
    <div class="form">
      <!--login form start-->
      <form class="login-form" action="login process.php" method="post">
        <h2>Login</h2>
        <div class="icons">
          <a href="#"><i class="fab fa-facebook"></i></a>
          <a href="#"><i class="fab fa-google"></i></a>
          <a href="#"><i class="fab fa-twitter"></i></a>
        </div>
        <input type="text" name="" value="" placeholder="Username" required>
        <input type="password" name="" value="" placeholder="Password" required>
        <p class="text-center f-w-600"><a href="#">Mot de passe oublié?</p></a>
        <button type="submit" name="button">Login</button>
        <p class="options">Not Registered? <a href="#">Create an Account</a></p>
      </form>
      <!--login form end-->
    </div>
    <!--form area end-->

  </body>
</html>